package jrigney

import org.scalatest.FunSuite
import org.scalatest.Matchers

import org.http4s._
import org.http4s.dsl._

class HelloWorldServiceSuite extends FunSuite with Matchers {

  test("Routes"){
    val getHello = Request(Method.GET, uri("/hello/bob"))

    val expectedHeaders = Headers(Header("Content-Type", "text/plain; charset=UTF-8"), Header("Content-Length", "11"))

    val actual = Routes.hello(getHello).unsafePerformSync
    actual.status shouldBe Status.Ok
    actual.headers shouldBe expectedHeaders
  }


}
