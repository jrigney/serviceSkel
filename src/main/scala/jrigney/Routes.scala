package jrigney

import org.http4s._
import org.http4s.dsl._

object Routes {

  val hello = HttpService {
    case GET -> Root / "hello" / name => Ok(s"Hello, $name.")
  }

  val routes = hello
}
